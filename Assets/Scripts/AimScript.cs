﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AimScript : MonoBehaviour
{
    #region Variables
    private GameObject[] objects;
    public GameObject body;
    public GameObject firstPersonCam;
    private GameObject target, temp;
    [SerializeField]private Camera cam;

    [Tooltip("Tryb celowania 0-3")]
    private int aim_type = 0;
    [Tooltip("Czułość wskaźnika w trakcie spowolnienia [Tryb 2]")]
    public float look_sensitivity_in_slow_mode = 2;
    [Tooltip("Minimalny obszar w którym musi się znajdować wskaźnik, aby przyciągać do celu [Tryb 2 oraz 3]")]
    public float maxDistanceFromObject = 150;
    private float distance = 0;    
    private bool isAnalog,isWalk;    
    private float findtarget=0;

    public PlayerController playerController;

    #endregion
    public void Aim(bool isLookAnalog, bool isWalkAnalog,float findTargetOnSide, int startedAim)
    {
        isAnalog = isLookAnalog; isWalk = isWalkAnalog; findtarget = findTargetOnSide;
        objects = GameObject.FindGameObjectsWithTag("Enemy");
        if (aim_type == 3) PrepareForType4(findTargetOnSide, startedAim);
        else if(aim_type == 2)
        {
            AimType3();
        }
        else if(aim_type == 1)
        {
            AimType2();
        }
    }
    //spowalniany wskaźnik i powolne przyciąganie do celu
    void AimType2()
    {
        if (objects.Length == 0) return;

        Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);   //środek ekranu
        Vector2 targetpos = new Vector2(cam.WorldToScreenPoint(objects[0].transform.position).x, cam.WorldToScreenPoint(objects[0].transform.position).y);

        float min = Vector2.Distance(screenCenter, targetpos);  //dystans między punktem na ekranie powstałym z obiektu a środkiem
        int index = 0;  //index pierwszego elementu
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].GetComponent<EnemyInputStuff>().isVisible() == false) continue;
            targetpos = new Vector2(cam.WorldToScreenPoint(objects[i].transform.position).x, cam.WorldToScreenPoint(objects[i].transform.position).y);
            distance = Vector2.Distance(screenCenter, targetpos);
            if (distance <= min)
            {
                index = i;
                min = distance;
            }
        }

        targetpos = new Vector2(cam.WorldToScreenPoint(objects[index].transform.position).x, cam.WorldToScreenPoint(objects[index].transform.position).y);
        distance = Vector2.Distance(screenCenter, targetpos);
        if (distance <= 150)
        {
            if (objects[index] != null && IsObjectBehindAnother(objects[index].transform) == true)  return;
            target = objects[index];
            playerController.ChangeSensitivity(look_sensitivity_in_slow_mode);
            AimType4(objects[index],0.03f);
        }
        else
            playerController.ResetSensitivity();
        return;
    }
    //przyciąga wskaźnik gdy ten jest w okolicy celu
    void AimType3()
    {
        if (objects.Length == 0) return;

        Vector2 screenCenter=new Vector2(Screen.width/2,Screen.height/2);   //środek ekranu
        Vector2 targetpos = new Vector2(cam.WorldToScreenPoint(objects[0].transform.position).x, cam.WorldToScreenPoint(objects[0].transform.position).y);

        float min = Vector2.Distance(screenCenter, targetpos);  //dystans między punktem na ekranie powstałym z obiektu a środkiem
        int index = 0;  //index pierwszego elementu
        for(int i=0;i<objects.Length;i++)
        {
            if (objects[i].GetComponent<EnemyInputStuff>().isVisible() == false) continue;
            targetpos = new Vector2(cam.WorldToScreenPoint(objects[i].transform.position).x, cam.WorldToScreenPoint(objects[i].transform.position).y);
            distance = Vector2.Distance(screenCenter, targetpos);
            if (distance <= min)
            {
                index = i;
                min = distance;
            }                
        }

        targetpos = new Vector2(cam.WorldToScreenPoint(objects[index].transform.position).x, cam.WorldToScreenPoint(objects[index].transform.position).y);
        distance = Vector2.Distance(screenCenter, targetpos);

        if (objects[index] != null && IsObjectBehindAnother(objects[index].transform) == true) return;//czy nie jest za innym obiektem
        if (distance <= maxDistanceFromObject)
        {
            target = objects[index];
            AimType4(objects[index], 0.3f);  //przybliżamy do celu, gdy jest w zasięgu
        }
        return;
    }
    //specyficzne dla pełnego wspomagania szukanie obiektu
    void PrepareForType4(float findTargetOnSide, int startedAim)
    {
        if (startedAim == 1) target = FindNearest(this.gameObject, 0);  //kiedy wsiśniemy klawisz celowania, to zawsze szukamy nowy obiekt
        else if (target == null) target = FindNearest(this.gameObject, 0);  //szukamy nowy, kiedy obiekt jest pusty
        else if (findTargetOnSide == 1)//na prawo
        {
            target = FindNearest(target, findTargetOnSide);
        }
        else if (findTargetOnSide == -1) //na lewo
        {
            target = FindNearest(target, findTargetOnSide);
        }
        if (target != null && IsObjectBehindAnother(target.transform) == true) return;
        if (target!=null && target.GetComponent<EnemyInputStuff>().isVisible()) AimType4(target, 0.3f);   //jeżeli obiekt jest w polu widzeniu gracza
        else
        {
            //jeżeli znaleziony obiekt nie jest w polu widzeniu gracza, to szukamy nowego
            Debug.Log("Obiekt jest niewidoczny");
            target = FindNearest(this.gameObject, 0);
        }
    }
    //pełne wspomaganie object_pos - obiekt, do którego celujemy    speed - z jaką prędkością
    void AimType4(GameObject object_pos, float speed)
    {
        if (object_pos == null) return;
        Vector3 targetPosition = object_pos.transform.position;
        targetPosition.y = transform.position.y;
        Quaternion rotation = Quaternion.LookRotation(targetPosition - transform.position);

        //czy już wycelowaliśmy
        RaycastHit hit;
        if (Physics.Raycast(firstPersonCam.GetComponent<Camera>().transform.position, firstPersonCam.GetComponent<Camera>().transform.forward, out hit, 100f) 
            && hit.collider.gameObject.CompareTag("Enemy") && isAnalog==false && isWalk==true)
        {
            body.transform.LookAt(targetPosition);
            firstPersonCam.transform.LookAt(object_pos.transform.position);
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed);
            targetPosition = object_pos.transform.position;
            rotation = Quaternion.LookRotation(targetPosition - firstPersonCam.transform.position);

            firstPersonCam.transform.rotation = Quaternion.Slerp(firstPersonCam.transform.rotation, rotation, speed);
            Vector3 temp = firstPersonCam.transform.localRotation.eulerAngles;
            temp.y = 0; temp.z = 0;
            firstPersonCam.transform.localRotation = Quaternion.Euler(temp);
        }

    }

    // from- obiekt, od którego szukamy najbliższego innego     side - po której stronie 1-po prawej, -1 - po lewej
    GameObject FindNearest(GameObject from, float side)
    {
        if (objects.Length == 0)
        {
            Debug.Log("Brak obiektow do namierzenia");
            return null;
        }
        GameObject a = objects[0].name != from.name ? objects[0] : objects[1];
        float min=Vector3.Distance(a.transform.position, from.transform.position);
        int index = objects[0].name != from.name ? 0 : 1;
        if(side!=0)
        {
            for(int i = 0; i < objects.Length; i++)
            {
                if (objects[i].name == from.name) continue;// jezeli trafiliśmy na ten sam, to pomijamy
                if (IsObjectBehindAnother(objects[i].transform) == true) continue;
                if (objects[i].GetComponent<EnemyInputStuff>().isVisible() == false) continue;
                if (ObjectIsOnScreenSide(from.transform, objects[i].transform, side))
                {
                    min = Vector3.Distance(from.transform.position, objects[i].transform.position);
                    index = i;
                    break;
                }
            }
        }
        
        for(int i=0;i<objects.Length;i++)
        {
            if (objects[i].name == from.name) continue;// jezeli trafiliśmy na ten sam, to pomijamy
            if (objects[i].GetComponent<EnemyInputStuff>().isVisible() == false) continue;
            if (IsObjectBehindAnother(objects[i].transform) == true) continue;
            float distance = Vector3.Distance(objects[i].transform.position, from.transform.position);
            if (distance < min && ObjectIsOnScreenSide(from.transform, objects[i].transform,side))
            {
                min = distance;
                index = i;
            }
        }
        if (objects[index].GetComponent<EnemyInputStuff>().isVisible() == false) return null;
        if (IsObjectBehindAnother(objects[index].transform) == true) return null;
        if (side!=0)
        {
            //jezeli nie znajdziemy po wybranej stronie ekranu
            if (ObjectIsOnScreenSide(from.transform,objects[index].transform, side) == false) return from;
        }
        return objects[index];
    }
    //czy znaleziony obiekt jest po żadanej stronie obiektu from
    bool ObjectIsOnScreenSide(Transform from,Transform to, float a)
    {
        if (a == 0) return true;
        // 1 - prawa strona, -1 - lewa strona
        from.transform.rotation = this.transform.rotation;
        Vector3 relativePoint = from.InverseTransformPoint(to.position);
        return relativePoint.x < 0.0 && a == -1 ? true : relativePoint.x > 0.0 && a == 1 ? true : false;
    }
    //true - jest za jakimś innym obiektem
    bool IsObjectBehindAnother(Transform to)
    {
        if (to == null) return true;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, to.position - transform.position, out hit, 1000f))
        {
            if (hit.collider != null && hit.collider.gameObject != null && hit.collider.gameObject.CompareTag("Enemy") == false)
            return true;
        }
        return false;
    }
    public bool TargetIsreachable()
    {
        return target!=null?!IsObjectBehindAnother(target.transform):false;
    }
    public int GetAimType()
    {
        return aim_type;
    }
    public void SetAimType(int value)
    {
        aim_type=value;
    }
}
